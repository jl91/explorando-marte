import { Application } from './src/application/application';

const app = new Application();

const port = 3000;

app.run(port);