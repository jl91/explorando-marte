import { Controller } from "./controller.interface";
import { Application } from "../application/application";

export abstract class AbstractController implements Controller {
    public application: Application;

    constructor(application: Application) {
        this.application = application;
    }
}