import { IndexController } from "./index.controller";

export const ControllerContainer = {} as any;

ControllerContainer[IndexController.constructor.name] = IndexController;