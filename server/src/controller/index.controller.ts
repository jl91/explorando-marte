import { AbstractController } from "./abstract.controller";

export class IndexController extends AbstractController {

    public indexAction(request: Request, response: Response | any) {
        response.send({
            'message': 'Hello World! 1'
        });
    }
}