import 'reflect-metadata';
import express from 'express';
import { RoutesConfig } from "../route/route.config";
import { Route } from "../route/route";
import { Controller } from "../controller/controller.interface";
import { ControllerContainer } from '../controller/controller-container.map'
import { Injectable } from "ts-di";

@Injectable()
export class Application {
    public express: any;
    private router: any;
    private controllersMap = new Map<string, Controller>();

    constructor() {
        this.express = express();
        this.router = express.Router();
        this.mountRoutes();
    }

    public run(port: number) {
        this.express
            .listen(port, (error: any) => {

                if (error) {
                    return console.log(error);
                }

                return console.log(`server is listening on ${port}`)

            });
    }

    private registerMiddlewareBefore(): void {
        this.express.use('/', this.router);
    }

    private mountRoutes(): void {
        const routesConfig = new RoutesConfig();

        this.registerMiddlewareBefore();

        routesConfig.routes
            .forEach((route: Route) => {
                let controller = this.instantiateController(route);
                for (let method of route.methods) {
                    this.registerRoute(method, route, controller);
                }
            });

        this.registerMiddlewareAfter();
    }

    private registerRoute(method: string, route: Route, controller: Controller | any): void {

        this.router[method.toLowerCase()](
            route.path,
            (request: Request, response: Response) => {
                return controller[route.action](request, response);
            }
        );
    }

    private registerMiddlewareAfter(): void {
        this.express.use((request: Request, response: Response) => {
            response.body;
        });
    }

    private instantiateController(route: Route): Controller {
        const controllerClass = route.controller;
        const instance = this;

        const controller = this.controllersMap.has(controllerClass) ?
            this.controllersMap.has(controllerClass) :
            new ControllerContainer[controllerClass](instance);

        if (!this.controllersMap.has(controllerClass)) {
            this.controllersMap.set(controllerClass, controller);
        }

        return controller;
    }
}