import { Route } from "./route";
import { IndexController } from "../controller/index.controller";

export class RoutesConfig {

    public routes: Array<Route>;

    constructor() {
        this.routes = [
            new Route("/", IndexController.constructor.name, 'indexAction', ["GET"]),
        ];
    }
}
