import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PlatformService } from "./service/platform.service";
import { SpaceshipService } from "./service/spaceship.service";

@Component({
    selector: 'app-platform',
    templateUrl: './platform.component.html',
    styleUrls: ['./platform.component.scss']
})
export class PlatformComponent implements OnInit {

    @ViewChild('mainCanvas')
    public mainCanvas: ElementRef;

    constructor(
        private platformService: PlatformService,
        private spaceshipService: SpaceshipService
    ) {
    }

    ngOnInit() {
        this.platformService.init(this.mainCanvas);
    }

    turnLeft(): void {
        this.spaceshipService.turnLeft();

    }

    move(): void {
        this.spaceshipService.move();
    }

    turnRight(): void {
        this.spaceshipService.turnRight();
    }

}
