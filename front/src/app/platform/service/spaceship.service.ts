import { ElementRef, Injectable } from '@angular/core';
import { DrawableInterface } from "./drawable.interface";
import { SpaceshipEnum } from "../enum/spaceship.enum";
import { Subject } from "rxjs";
import { CardinalDirectionEnum } from "../enum/cardinal-direction.enum";

@Injectable()
export class SpaceshipService implements DrawableInterface {
    private elementRef: ElementRef;
    private context: CanvasRenderingContext2D;
    private canvasWidth: number = 0;
    private canvasHeight: number = 0;
    private image: HTMLImageElement;
    private spriteLoaderSubject = new Subject<boolean>();
    private coordinates = {x: 100, y: 100, direction: CardinalDirectionEnum.NORTH};
    private readonly MINIMUM_VALUE = 0;
    private readonly TO_RADIANS = Math.PI / 180;
    private shouldRotateImage: boolean = false;


    constructor() {
        this.registerOnSpriteLoaderSubject();
    }

    init(elementRef: ElementRef): void {
        this.elementRef = elementRef;
        this.canvasWidth = this.elementRef.nativeElement.width;
        this.canvasHeight = this.elementRef.nativeElement.height;
        this.context = elementRef.nativeElement.getContext('2d');
        this.loadSprite();
    }

    draw(): void {
        const ctx = this.context;
        ctx.beginPath();

        if (!this.shouldRotateImage) {
            this.drawImage(this.coordinates.x, this.coordinates.y);
        } else {
            this.rotate();
        }
        ctx.fill();
        ctx.closePath();
    }

    turnLeft(): void {
        this.shouldRotateImage = true;
        this.angleToLeft();
        this.rotate();
    }

    move(): void {
        this.shouldRotateImage = true;
        this.erase();
        this.nextPosition();
        this.draw();
    }

    turnRight(): void {
        this.shouldRotateImage = true;
        this.angleToRight();
        this.rotate();
    }

    private drawImage(x, y): void {
        this.context.drawImage(this.image, x, y, SpaceshipEnum.WIDTH, SpaceshipEnum.HEIGHT);
    }

    private rotate(): void {
        this.erase();
        const ctx = this.context;
        ctx.save();
        const x = SpaceshipEnum.WIDTH / 2;
        const y = SpaceshipEnum.HEIGHT / 2;
        ctx.beginPath();
        ctx.translate(this.coordinates.x, this.coordinates.y);
        ctx.translate(x, y);
        ctx.rotate(this.getAngle() * this.TO_RADIANS);
        this.drawImage(x * -1, y * -1);
        ctx.restore();
        ctx.fill();
        ctx.closePath();
    }

    private angleToLeft(): void {
        switch (this.coordinates.direction) {
            case CardinalDirectionEnum.NORTH:
                this.coordinates.direction = CardinalDirectionEnum.WEST;
                break;
            case CardinalDirectionEnum.WEST:
                this.coordinates.direction = CardinalDirectionEnum.SOUTH;
                break;
            case CardinalDirectionEnum.SOUTH:
                this.coordinates.direction = CardinalDirectionEnum.EAST;
                break;
            case CardinalDirectionEnum.EAST:
                this.coordinates.direction = CardinalDirectionEnum.NORTH;
                break;
        }
    }

    private angleToRight(): void {
        switch (this.coordinates.direction) {
            case CardinalDirectionEnum.NORTH:
                this.coordinates.direction = CardinalDirectionEnum.EAST;
                break;
            case CardinalDirectionEnum.EAST:
                this.coordinates.direction = CardinalDirectionEnum.SOUTH;
                break;
            case CardinalDirectionEnum.SOUTH:
                this.coordinates.direction = CardinalDirectionEnum.WEST;
                break;
            case CardinalDirectionEnum.WEST:
                this.coordinates.direction = CardinalDirectionEnum.NORTH;
                break;
        }
    }

    private getAngle(): number {
        switch (this.coordinates.direction) {
            case CardinalDirectionEnum.NORTH:
                return CardinalDirectionEnum.NORTH_ANGLE;
            case CardinalDirectionEnum.EAST:
                return CardinalDirectionEnum.EAST_ANGLE;
            case CardinalDirectionEnum.SOUTH:
                return CardinalDirectionEnum.SOUTH_ANGLE;
            case CardinalDirectionEnum.WEST:
                return CardinalDirectionEnum.WEST_ANGLE;
        }
    }

    private increaseYWith(spaceshipEnum: SpaceshipEnum): void {
        let nextValue = this.coordinates.y;
        nextValue += spaceshipEnum;

        if (
            nextValue >= this.MINIMUM_VALUE &&
            nextValue <= this.canvasHeight - spaceshipEnum
        ) {
            this.coordinates.y = nextValue;
        }
    }

    private decreaseYWith(spaceshipEnum: SpaceshipEnum): void {
        let nextValue = this.coordinates.y;
        nextValue -= spaceshipEnum;

        if (
            nextValue >= this.MINIMUM_VALUE &&
            nextValue <= this.canvasHeight - spaceshipEnum
        ) {
            this.coordinates.y = nextValue;
        }
    }

    private increaseXWith(spaceshipEnum: SpaceshipEnum): void {
        let nextValue = this.coordinates.x;
        nextValue += spaceshipEnum;

        if (
            nextValue >= this.MINIMUM_VALUE &&
            nextValue <= this.canvasHeight - spaceshipEnum
        ) {
            this.coordinates.x = nextValue;
        }
    }

    private decreaseXWith(spaceshipEnum: SpaceshipEnum): void {
        let nextValue = this.coordinates.x;
        nextValue -= spaceshipEnum;

        if (
            nextValue >= this.MINIMUM_VALUE &&
            nextValue <= this.canvasHeight - spaceshipEnum
        ) {
            this.coordinates.x = nextValue;
        }
    }

    private nextPosition(): void {
        switch (this.coordinates.direction) {
            case CardinalDirectionEnum.NORTH:
                this.decreaseYWith(SpaceshipEnum.HEIGHT);
                break;
            case CardinalDirectionEnum.EAST:
                this.increaseXWith(SpaceshipEnum.WIDTH);
                break;
            case CardinalDirectionEnum.SOUTH:
                this.increaseYWith(SpaceshipEnum.HEIGHT);
                break;
            case CardinalDirectionEnum.WEST:
                this.decreaseXWith(SpaceshipEnum.WIDTH);
                break;
        }
    }

    private erase(): void {
        const ctx = this.context;
        ctx.clearRect(
            0,
            0,
            this.canvasWidth,
            this.canvasHeight
        );
    }

    private loadSprite(): void {
        this.image = new Image(SpaceshipEnum.WIDTH, SpaceshipEnum.HEIGHT);
        this.image.src = '/assets/images/spaceship.svg';
        this.image.onload = () => this.spriteLoaderSubject.next(true);
    }

    private registerOnSpriteLoaderSubject(): void {
        this.spriteLoaderSubject
            .subscribe(() => {
                this.draw();
            });
    }
}
