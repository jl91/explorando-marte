import { ElementRef } from "@angular/core";

export interface DrawableInterface {
    init(elementRef: ElementRef): void;

    draw(): void;
}
