import { ElementRef, Injectable } from '@angular/core';
import { SpaceshipService } from "./spaceship.service";
import { DrawableInterface } from "./drawable.interface";

@Injectable()
export class PlatformService implements DrawableInterface {

    private elementRef: ElementRef;
    private context: CanvasRenderingContext2D;
    private readonly canvasWidth = 400;
    private readonly canvasHeight = 400;


    constructor(private spaceshipService: SpaceshipService) {
    }

    init(elementRef: ElementRef): void {
        this.elementRef = elementRef;
        this.elementRef.nativeElement.width = this.canvasWidth;
        this.elementRef.nativeElement.height = this.canvasHeight;
        this.context = elementRef.nativeElement.getContext('2d');

        this.spaceshipService.init(elementRef);
        this.draw();
    }

    draw(): void {
        this.spaceshipService.draw();
    }

}
