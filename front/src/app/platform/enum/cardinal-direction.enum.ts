export enum CardinalDirectionEnum {
    NORTH = 'n',
    EAST = 'e',
    SOUTH = 's',
    WEST = 'w',
    NORTH_ANGLE = 0,
    EAST_ANGLE = 90,
    SOUTH_ANGLE = 180,
    WEST_ANGLE = 270
}
