import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlsService } from "./service/controls.service";

@NgModule({
    declarations: [
        ControlsService
    ],
    imports: [
        CommonModule
    ]
})
export class SharedModule {
}
