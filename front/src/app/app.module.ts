import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './header/header.component';
import { PlatformComponent } from './platform/platform.component';
import { PlatformService } from "./platform/service/platform.service";
import { SpaceshipService } from "./platform/service/spaceship.service";


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        PlatformComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [
        PlatformService,
        SpaceshipService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
